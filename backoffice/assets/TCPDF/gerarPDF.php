<?php
require_once 'tcpdf.php';

function recolheDados()
{
    session_start();
    $sql = $_SESSION['sqlPDF'];
    $bd = null;
    $saida = null;

    include_once '../../php/connectDB.php';

    $result = $bd->query($sql);

    while ($row = $result->fetch_assoc()) {
        switch ($row['estado']) {
            case 0:
                $row['estado'] = 'Falecido';
                $trClass = 'class="table-danger"';
                break;
            case 1:
                $row['estado'] = 'Velho';
                $trClass = 'class="table-light"';
                break;
            case 2:
                $row['estado'] = 'Activo';
                $trClass = 'class="table-primary"';
                break;
        }
        $saida .= '<tr>
            <td width="20">' . $row['ano_entrada'] . '</td>
            <td width="75">' . $row['alcunha'] . '</td>
            <td width="160">' . $row['nome'] . '</td>
            <td width="160">' . $row['email'] . '</td>
            <td width="55">' . $row['telefone1'] . '</td>
            <td width="55">' . $row['telefone2'] . '</td>
            <td width="150">' . $row['rua'] . '<br>' . $row['cod_postal'] . ' ' . $row['localidade'] . ' ' . $row['pais'] . '</td>
            <td width="35">' . $row['estado'] . '</td>
            <td width="40">' . $row['cc'] . '</td>
        </tr>';
    }

    return $saida;
}

$dataActual = date_default_timezone_get() . ' ' . date('d/m/Y H:i:s');;

$pdf = new TCPDF('L', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$pdf->setCreator(PDF_CREATOR);
$pdf->setTitle("Base de dados da Orxestra Pitagórica");
$pdf->setHeaderData('', '', PDF_HEADER_TITLE, PDF_HEADER_STRING);
$pdf->setHeaderFont(array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$pdf->setFooterFont(array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$pdf->setDefaultMonospacedFont('helvetica');
$pdf->setFooterMargin(PDF_MARGIN_FOOTER);
$pdf->setMargins(PDF_MARGIN_LEFT, '10', PDF_MARGIN_RIGHT);
$pdf->setPrintHeader(false);
$pdf->setAutoPageBreak(true, 10);
$pdf->setFont('helvetica', '', 8);

$pdf->AddPage();

$conteudo = '';
$conteudo .= '
<style>
td, th {
    border: 1px solid lightgrey;
    text-align: center;
    align-items: center;
}
</style>
<h1>Orxestra Pitagórica</h1>
<h2>Contactos</h2>
<p>Data: ' . $dataActual . '</p>
<table>
    <thead>
    <tr>
        <th width="20">Ano</th>
        <th width="75">Alcunha</th>
        <th width="160">Nome</th>
        <th width="160">Email</th>
        <th width="55">Telefone 1</th>
        <th width="55">Telefone 2</th>
        <th width="150">Morada</th>
        <th width="35">Estado</th>
        <th width="40">CC</th>
    </tr>
    </thead>
    <tbody>
';
$conteudo .= recolheDados();
$conteudo .= '</tbody></table>';

$pdf->writeHTML($conteudo);

$pdf->Output('bdOrxestra.pdf', 'I');
?>