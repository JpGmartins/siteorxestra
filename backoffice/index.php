<?php
session_start();
//Destroi qualquer sessao ativa
if (isset($_SESSION["idUtilizador"])) {
//    Insere o registo de logout na tabela
    require 'php/connectDB.php';

    $agente = $_SERVER['HTTP_USER_AGENT'];
    $sql = "INSERT INTO orxestra_pitagorica.log (id_utilizador, data_logout, agente) VALUES (" . $_SESSION['idUtilizador'] . ", current_time(), '".$agente."')";

    $bd->query($sql);
    $bd->close();

//    Destroi todas as sessões activas
    session_unset();
    session_destroy();
}

require 'php/functions.php'; ?>
<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="João Pedro Martins">
    <meta name="publisher" content="Secção de Fado da Associação Académica de Coimbra">

    <!----------Bootstrap--------->
    <link rel="stylesheet" href="../assets/css/bootstrap.min.css">

    <!--------Stylesheet personalizada------>
    <link rel="stylesheet" href="assets/css/signin.css">

    <link rel="icon" href="../img/wc.png">
    <title>Login</title>

    <style>
        @media (min-width: 768px) {
            .bd-placeholder-img-lg {
                font-size: 3.5rem;
            }
        }
    </style>
</head>

<body class="text-center">
<?php
//Verifica se a variavel que diz que o e-mail não existe na bd esta definida e envia um alerta para o ecrã a informar o utilizador
if (isset($_GET["naoExiste"])) {
    echo "<script>alert(\"O utilizador não existe!\");</script>";
}

//Verifica se a variavel que diz que a palavra passe esta errada esta definida e envia um alerta para informar o utilizador
if (isset($_GET["palavraPasseErrada"])) {
    echo "<script>alert(\"A palavra-passe está errada.\");</script>";
}
?>
<main class="form-signin">
    <form action="#" method="POST">
        <img class="mb-4 rounded" src="../img/logoOP.png" alt="" width="150">
        <h1 class="h3 mb-3 fw-normal">Faça o login</h1>
        <div class="form-floating">
            <input type="text" class="form-control" id="alcunha" name="alcunha" placeholder="Curto">
            <label for="alcunha">Alcunha</label>
        </div>
        <div class="form-floating">
            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
            <label for="password">Password</label>
        </div>
        <button class="w-100 btn btn-lg btn-primary" type="submit" name="login">Login</button>
        <button class="w-100 btn btn-lg btn-secondary mt-1" type="button" onclick="location.href='../index.html'">
            orxestrapitagorica.pt
        </button>
        <p class="mt-5 mb-3 text-muted">&copy; 2022</p>
    </form>
</main>
</body>

</html>