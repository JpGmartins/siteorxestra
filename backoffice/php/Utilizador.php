<?php

class Utilizador
{
    public $idUtilizador;
    public $alcunha;
    public $nome;
    public $apelido;
    public $email;
    public $nivelAcesso;
    public $pass;
    public $img;
    public $activo;

    /**
     * @param $idUtilizador
     * @param $alcunha
     * @param $nome
     * @param $apelido
     * @param $email
     * @param $nivelAcesso
     * @param $pass
     * @param $img
     * @param $activo
     */
    public function __construct($idUtilizador, $alcunha, $nome, $apelido, $email, $nivelAcesso, $pass, $img, $activo)
    {
        $this->idUtilizador = $idUtilizador;
        $this->alcunha = $alcunha;
        $this->nome = $nome;
        $this->apelido = $apelido;
        $this->email = $email;
        $this->nivelAcesso = $nivelAcesso;
        $this->pass = $pass;
        $this->img = $img;
        $this->activo = $activo;
    }

    /**
     * @return mixed
     */
    public function getIdUtilizador()
    {
        return $this->idUtilizador;
    }

    /**
     * @return mixed
     */
    public function getAlcunha()
    {
        return $this->alcunha;
    }

    /**
     * @return mixed
     */
    public function getNome()
    {
        return $this->nome;
    }

    /**
     * @return mixed
     */
    public function getApelido()
    {
        return $this->apelido;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @return mixed
     */
    public function getNivelAcesso()
    {
        return $this->nivelAcesso;
    }

    /**
     * @return mixed
     */
    public function getPass()
    {
        return $this->pass;
    }

    /**
     * @return mixed
     */
    public function getImg()
    {
        return $this->img;
    }

    /**
     * @return mixed
     */
    public function getActivo()
    {
        return $this->activo;
    }
}

include "connectDB.php";

$sql = "SELECT * FROM orxestra_pitagorica.utilizadores WHERE id_utilizador = " . $_SESSION['idUtilizador'];
$result = $bd->query($sql);
$row = mysqli_fetch_assoc($result);

$utilizador = new Utilizador($row['id_utilizador'], $row['alcunha'], $row['nome'], $row['apelido'], $row['email'], $row['nivel_acesso'], $row['pass'], $row['img'], $row['activo']);