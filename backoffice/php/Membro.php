<?php

class Membro
{
    public $idMembro;
    public $alcunha;
    public $nome;
    public $anoEntrada;
    public $cc;
    public $email;
    public $telefone1;
    public $telefone2;
    public $rua;
    public $codPostal;
    public $localidade;
    public $pais;
    public $estado;
    public $descricao;
    public $fotoPerfil;

    public function __construct($idMembro, $alcunha, $nome, $anoEntrada, $cc, $email, $telefone1, $telefone2, $rua, $codPostal, $localidade, $pais, $estado, $descricao, $fotoPerfil)
    {
        $this->idMembro = $idMembro;
        $this->alcunha = $alcunha;
        $this->nome = $nome;
        $this->anoEntrada = $anoEntrada;
        $this->cc = $cc;
        $this->email = $email;
        $this->telefone1 = $telefone1;
        $this->telefone2 = $telefone2;
        $this->rua = $rua;
        $this->codPostal = $codPostal;
        $this->localidade = $localidade;
        $this->pais = $pais;
        $this->estado = $estado;
        $this->descricao = $descricao;
        $this->fotoPerfil = $fotoPerfil;
    }

    public function getIdMembro()
    {
        return $this->idMembro;
    }

    public function getAlcunha()
    {
        return $this->alcunha;
    }

    public function getNome()
    {
        return $this->nome;
    }

    public function getAnoEntrada()
    {
        return $this->anoEntrada;
    }

    public function getcc()
    {
        return $this->cc;
    }

    public function getEmail()
    {
        return $this->email;
    }

    public function getTelefone1()
    {
        return $this->telefone1;
    }

    public function getTelefone2()
    {
        return $this->telefone2;
    }

    public function getRua()
    {
        return $this->rua;
    }

    public function getCodPostal()
    {
        return $this->codPostal;
    }

    public function getLocalidade()
    {
        return $this->localidade;
    }

    public function getPais()
    {
        return $this->pais;
    }

    public function getEstado()
    {
        return $this->estado;
    }

    public function getDescricao()
    {
        return $this->descricao;
    }

    public function getFotoPerfil()
    {
        return $this->fotoPerfil;
    }
}

include 'connectDB.php';

$sql = "SELECT * FROM orxestra_pitagorica.membros WHERE id_membro = " . $_SESSION['idMembro'];
$result = $bd->query($sql);
$row = mysqli_fetch_assoc($result);

$membro = new Membro($row['id_membro'], $row['alcunha'], $row['nome'], $row['ano_entrada'], $row['cc'], $row['email'], $row['telefone1'], $row['telefone2'], $row['rua'], $row['cod_postal'], $row['localidade'], $row['pais'], $row['estado'], $row['descricao'], $row['img']);