<?php

function eliminaMembro()
{
    $idMembro = $_SESSION['idMembro'];
    $bd = null;
    $sql = "DELETE FROM orxestra_pitagorica.membros WHERE (id_membro = '$idMembro');";

    require_once 'connectDB.php';

    try {
        $bd->query($sql);
        $bd->close();
        header("location: membros.php");
    } catch (Exception $e) {
        echo $e;
        return false;
    }
}

function editaMembro(): bool
{
    $bd = null;
    $alcunha = $_POST['alcunha'];
    $nome = $_POST['nome'];
    $anoEntrada = $_POST['anoEntrada'];
    if ($_POST['cc'] != "")
        $cc = $_POST['cc'];
    else
        $cc = "null";
    $estado = $_POST['estado'];
    $email = $_POST['email'];
    $tel1 = $_POST['tel1'];
    $tel2 = $_POST['tel2'];
    $rua = $_POST['rua'];
    $codPostal = $_POST['codPostal'];
    $localidade = $_POST['localidade'];
    $pais = $_POST['pais'];
    $descricao = $_POST['descricao'];
    $sql = "UPDATE orxestra_pitagorica.membros SET alcunha=\"" . $alcunha . "\", nome=\"" . $nome . "\", ano_entrada=\"" . $anoEntrada . "\", cc=" . $cc . ", email=\"" . $email . "\", telefone1=\"" . $tel1 . "\", telefone2=\"" . $tel2 . "\", rua=\"" . $rua . "\", cod_postal=\"" . $codPostal . "\", localidade=\"" . $localidade . "\", pais=\"" . $pais . "\", estado=\"" . $estado . "\", descricao=\"" . $descricao . "\" WHERE id_membro = " . $_SESSION['idMembro'];

    if (isset($_SESSION['nomeIMG']))
        $sql = "UPDATE orxestra_pitagorica.membros SET alcunha=\"" . $alcunha . "\", nome=\"" . $nome . "\", ano_entrada=\"" . $anoEntrada . "\", cc=" . $cc . ", email=\"" . $email . "\", telefone1=\"" . $tel1 . "\", telefone2=\"" . $tel2 . "\", rua=\"" . $rua . "\", cod_postal=\"" . $codPostal . "\", localidade=\"" . $localidade . "\", pais=\"" . $pais . "\", estado=\"" . $estado . "\", descricao=\"" . $descricao . "\", img=\"" . $_SESSION['nomeIMG'] . "\" WHERE id_membro = " . $_SESSION['idMembro'];

    require '../php/connectDB.php';

    try {
        $bd->query($sql);
        $bd->close();
        echo '<script>alert("Sucesso")</script>';
        return true;
    } catch (Exception $e) {
        echo $e->getMessage();
        echo $sql;
        $bd->close();
        return false;
    }
}

function registarMembro(): bool
{
    $bd = "";
    $alcunha = $_POST['alcunha'];
    $nome = $_POST['nome'];
    $anoEntrada = $_POST['anoEntrada'];
    if ($_POST['cc'] != "")
        $cc = $_POST['cc'];
    else
        $cc = "null";
    $estado = $_POST['estado'];
    $email = $_POST['email'];
    $tel1 = $_POST['tel1'];
    $tel2 = $_POST['tel2'];
    $rua = $_POST['rua'];
    $codPostal = $_POST['codPostal'];
    $localidade = $_POST['localidade'];
    $pais = $_POST['pais'];
    if ($_POST['descricao'] != "")
        $descricao = $_POST['descricao'];
    else
        $descricao = "null";
    $sql = "INSERT INTO orxestra_pitagorica.membros (alcunha, nome, ano_entrada, cc, email, telefone1, telefone2, rua, cod_postal, localidade, pais, estado, descricao, img) VALUES ('" . $alcunha . "','" . $nome . "'," . $anoEntrada . "," . $cc . ",'" . $email . "',\"" . $tel1 . "\",\"" . $tel2 . "\",'" . $rua . "','" . $codPostal . "','" . $localidade . "','" . $pais . "'," . $estado . ",'" . $descricao . "', '../system/img/person.svg')";

    session_start();
//    Verifica se houve upload de imagem
    if (isset($_SESSION["nomeIMG"])) {
        $sql = "INSERT INTO orxestra_pitagorica.membros (alcunha, nome, ano_entrada, cc, email, telefone1, telefone2, rua, cod_postal, localidade, pais, estado, descricao, img) VALUES ('" . $alcunha . "','" . $nome . "'," . $anoEntrada . "," . $cc . ",'" . $email . "',\"" . $tel1 . "\",\"" . $tel2 . "\",'" . $rua . "','" . $codPostal . "','" . $localidade . "','" . $pais . "'," . $estado . ",'" . $descricao . "', '" . $_SESSION['nomeIMG'] . "')";
        $_SESSION['nomeIMG'] = null;
    }

    require_once 'connectDB.php';

    try {
        $bd->query($sql);
        $bd->close();
        return true;
    } catch (Exception $e) {
        echo $e->getMessage();
        echo $sql;
        $bd->close();
        return false;
    }
}

function registarUtilizador(): bool
{
    $nome = $_POST['nome'];
    $apelido = $_POST['apelido'];
    $alcunha = $_POST['alcunha'];
    $email = $_POST['email'];
    $pass = $_POST['pass'];
    $pass = password_hash($_POST['pass'], PASSWORD_DEFAULT);
    $nivelAcesso = $_POST['admin'];
    $bd = null;
    if ($nivelAcesso == null)
        $nivelAcesso = 0;
    if (isset($_SESSION['nomeIMG'])) {
        $img = $_SESSION['nomeIMG'];
        $sql = "INSERT INTO orxestra_pitagorica.utilizadores (email, pass, alcunha, nome, apelido, nivel_acesso, img, activo) VALUES ('$email', '$pass', '$alcunha', '$nome', '$apelido', '$nivelAcesso', '$img', true)";
    } else
        $sql = "INSERT INTO orxestra_pitagorica.utilizadores (email, pass, alcunha, nome, apelido, nivel_acesso, img, activo) VALUES ('$email', '$pass', '$alcunha', '$nome', '$apelido', '$nivelAcesso', '../system/img/person.svg', true)";

    include 'connectDB.php';

    try {
        $bd->query($sql);
        $bd->close();
        header('location: utilizadores.php');
    } catch (Exception $e) {
        return false;
        echo $e->getMessage();
    }
}


function login()
{
    $bd = null;
    $alcunha = $_POST['alcunha'];
    $password = $_POST['password'];
    $sql = "SELECT id_utilizador, alcunha, pass, nivel_acesso, activo FROM utilizadores WHERE alcunha LIKE '$alcunha'";

    //Conecção à Base de dados
    require 'connectDB.php';

    //Guarda variaveis num array $result
    $result = $bd->query($sql);
    $row = $result->fetch_assoc();

    //verificação de início de sessão com hash
    if ($result->num_rows == 1 && $row['activo']) {
        if (password_verify($password, $row['pass'])) {
            session_start();
            $_SESSION['idUtilizador'] = $row['id_utilizador'];
            $_SESSION['alcunha'] = $row['alcunha'];
            $_SESSION['acesso'] = $row['nivel_acesso'];
            $agente = $_SERVER['HTTP_USER_AGENT'];

            $sql = "INSERT INTO orxestra_pitagorica.log (id_utilizador, data_login, agente) VALUES (" . $_SESSION['idUtilizador'] . ", current_time(), '" . $agente . "')";
            $bd->query($sql);

            $bd->close();

            header("location: system/home.php");

        } else {
            $bd->close();
            header('location: ?palavraPasseErrada=true');
        }
    } else if ($result->num_rows == 0) {
        $bd->close();
        header('location: ?naoExiste=true');
    }
}

function pesquisa()
{
    $bd = null;
    $conteudo = null;
    if (isset($_POST['estadoFalecido']))
        $estadoFalecido = $_POST['estadoFalecido'];
    if (isset($_POST['estadoVelho']))
        $estadoVelho = $_POST['estadoVelho'];
    if (isset($_POST['estadoActivo']))
        $estadoActivo = $_POST['estadoActivo'];
    if (isset($_POST['pesquisa']))
        if ($_POST['pesquisa'] != "")
            $pesquisa = $_POST['pesquisa'];
    $sql = "SELECT * FROM orxestra_pitagorica.membros";
    $condicoes = array();

    if (isset($estadoFalecido))
        $condicoes[] = "estado='$estadoFalecido'";
    if (isset($estadoVelho))
        $condicoes[] = "estado='$estadoVelho'";
    if (isset($estadoActivo))
        $condicoes[] = "estado='$estadoActivo'";

    if (count($condicoes) > 0 and isset($pesquisa)) {
        $sql .= " WHERE " . implode(' OR ', $condicoes);
        $sql .= " AND (membros.ano_entrada LIKE '%$pesquisa%' OR membros.alcunha LIKE '%$pesquisa%' OR membros.nome LIKE '%$pesquisa%')";
    } elseif (count($condicoes) > 0 and !isset($pesquisa)) {
        $sql .= " WHERE " . implode(' OR ', $condicoes);
    } elseif (count($condicoes) == 0 and isset($pesquisa)) {
        $sql .= " WHERE membros.ano_entrada LIKE '%$pesquisa%' OR membros.alcunha LIKE '%$pesquisa%' OR membros.nome LIKE '%$pesquisa%'";
    }

    $_SESSION['sqlPDF'] = $sql;
    require 'connectDB.php';

    $result = $bd->query($sql);

    if ($result->num_rows > 0) {
        while ($row = $result->fetch_assoc()) {
            switch ($row['estado']) {
                case 0:
                    $row['estado'] = 'Falecido';
                    $trClass = 'class="table-danger"';
                    break;
                case 1:
                    $row['estado'] = 'Antigo';
                    $trClass = 'class="table-light"';
                    break;
                case 2:
                    $row['estado'] = 'Activo';
                    $trClass = 'class="table-primary"';
                    break;
            }

            $conteudo .= "
            <tr style=\"cursor: pointer\" " . $trClass . " onclick='location.href=\"perfil.php?idMembro=" . $row['id_membro'] . "\"'>
                <td>" . $row['ano_entrada'] . "</td>
                <td>" . $row['alcunha'] . "</td>
                <td>" . $row['nome'] . "</td>
                <td>" . $row['estado'] . "</td>
            </tr>";
        }
        $bd->close();
    } else {
        echo('<tr class="table-light text-center"><td colspan="4">Sem registos</td></tr>');
    }

    return $conteudo;
}

function novaPass()
{
    $novaPass = password_hash($_POST['novaPass'], PASSWORD_DEFAULT);

    require 'connectDB.php';

    $sql = "SELECT pass FROM orxestra_pitagorica.utilizadores WHERE id_utilizador LIKE \"" . $_SESSION['idUtilizador'] . "\"";

    $result = $bd->query($sql);
    $row = $result->fetch_assoc();

    if (password_verify($_POST['passActual'], $row['pass'])) {
        if (isset($_SESSION['nomeIMG'])) {
            $sql = "UPDATE orxestra_pitagorica.utilizadores SET pass = \"" . $novaPass . "\", img = \"" . $_SESSION['nomeIMG'] . "\" WHERE id_utilizador = " . $_SESSION['idUtilizador'];
        } else
            $sql = "UPDATE orxestra_pitagorica.utilizadores SET pass = \"" . $novaPass . "\" WHERE id_utilizador = " . $_SESSION['idUtilizador'];
        $bd->query($sql);
        $bd->close();
        header("Refresh:0");
    } else {
        $bd->close();
        echo "<script>alert(\"Pala-passe errada\")</script>";
    }

}

function imprimeUtilizadores(){
    require 'connectDB.php';

    $sql = "SELECT * FROM utilizadores WHERE activo = true";
    $result = $bd->query($sql);
    $conteudo = null;

    while ($row = $result->fetch_assoc()){
        switch ($row['nivel_acesso']){
            case 0:
                $admin = "Não";
                break;
            case 1:
                $admin = "Sim";
                break;
            default:
                $admin = "Não";
        }

        if($_SESSION['idUtilizador'] == $row['id_utilizador']){
            $conteudo .= '
            <div class="col-4">
                <div class="card mt-3">
                    <img src="'.$row['img'].'" class="card-img-top rounded mt-2" alt="Foto de Perfil" style="max-width: 150px; margin: auto">
                    <div class="card-body">
                        <h5 class="card-title">'.$row['alcunha'].'</h5>
                        <p class="card-text">'.$row['nome'].' '.$row['apelido'].'</p>
                        <p class="card-text">'.$row['email'].'</p>
                        <p class="card-text">Administrador: '.$admin.'</p>
                        <button disabled class="btn btn-danger" name="btnEliminaUtilizador" onclick="location.href=\'eliminaUtilizador.php?eliminaIdUtilizador='.$row['id_utilizador'].'\'">Eliminar</button>
                    </div>
                </div>
            </div>
            ';
        }else{
            $conteudo .= '
            <div class="col-4">
                <div class="card mt-3">
                    <img src="'.$row['img'].'" class="card-img-top rounded mt-2" alt="Foto de Perfil" style="max-width: 150px; margin: auto">
                    <div class="card-body">
                        <h5 class="card-title">'.$row['alcunha'].'</h5>
                        <p class="card-text">'.$row['nome'].' '.$row['apelido'].'</p>
                        <p class="card-text">'.$row['email'].'</p>
                        <p class="card-text">Administrador: '.$admin.'</p>
                        <button class="btn btn-danger" name="btnEliminaUtilizador" onclick="location.href=\'eliminaUtilizador.php?eliminaIdUtilizador='.$row['id_utilizador'].'\'">Eliminar</button>
                    </div>
                </div>
            </div>
            ';
        }
    }
    return $conteudo;
}

function eliminaUtilizador($idUtilizador){
    require 'connectDB.php';

    $sql = "UPDATE utilizadores SET activo = false WHERE id_utilizador = ".$idUtilizador;

    try {
        $bd->query($sql);
        $bd->close();
        return true;
    } catch (Exception $e) {
        echo $e;
        return false;
    }
}

if ($_GET) {
    if (isset($_GET['pesquisar'])) {
        pesquisa();
    }
}
if ($_POST) {
    if (isset($_POST['login'])) {
        login();
    } elseif (isset($_POST['btnEditaMembro'])) {
        if (editaMembro()) {
            header("location: perfil.php?idMembro=" . $_SESSION['idMembro']);
        } else {
            echo 'Ocorreu um erro inesperado';
        }
    } elseif (isset($_POST['btnRegistarMembro'])) {
        if (registarMembro()) {
            header('location: registarMembro.php?resultado=true');
        } else
            echo "Ocorreu um problema inesperado";
    } elseif (isset($_POST['btnEliminarMembro'])) {
        eliminaMembro();
    } elseif (isset($_POST['btnRegistarUtilizador'])) {
        if (registarUtilizador()) {
            echo("<script>alert(\"Utilizador inserido com sucesso.\")");
        } else
            echo "<script>alert(\"Ocorreu um problema inesperado.\")";
    } elseif (isset($_POST['submmitNovaPass'])) {
        novaPass();
    }
}
