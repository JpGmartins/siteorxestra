<?php
$servidor = 'localhost:3306';
$utilizadorBD = 'root';
$passBD = '';
$nomeBD = 'orxestra_pitagorica';

try {
    $bd = new mysqli($servidor, $utilizadorBD, $passBD, $nomeBD);
    $bd->set_charset("utf8");
} catch (Exception $e) {
    die("Connection failed: " . $bd->connect_error);
}

?>