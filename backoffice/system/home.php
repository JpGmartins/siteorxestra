<?php
session_start();

if (!isset($_SESSION['idUtilizador'])) {
    header('location: ../index.php');
}

include 'header.php';
?>

    <main>
        <div class="container">
            <div class="row">
                <h3 class="mt-5 mb-5">Bem vindo ao PitaOffice</h3>
            </div>
            <div class="row">
                <div class="col-12">
                    <p>Bem-vindo amigo <b><?php echo $utilizador->alcunha ?>!</b></p>
                    <p>Aqui podes consultar e
                        actualizar a base de dados da Orxestra Pitagórica. É altamente recomendade fazer um backup pelo menos uma vez por ano
                        para não haver risco de perda de dados.</p>
                    <p>Quaisquer dúvidas que possam surgir, basta contactar-me (Curto) para o
                        joaopedro.gomes.martins@gmail.com ou para o 966 100 332</p>
                    <h5>EaMt2B!</h5>
                </div>
            </div>
        </div>
    </main>
<?php
require 'footer.html';