<?php
$pagina = $_SERVER['REQUEST_URI'];
require '../php/Utilizador.php';
?>
<!DOCTYPE html>
<html lang="pt">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="author" content="João Pedro Martins">
    <meta name="publisher" content="Secção de Fado da Associação Académica de Coimbra">

    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Bebas+Neue&family=Source+Sans+Pro&display=swap"
          rel="stylesheet">

    <!--    CROP jQuery import -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>
    <link rel="stylesheet" href="https://unpkg.com/dropzone/dist/dropzone.css"/>
    <link href="https://unpkg.com/cropperjs/dist/cropper.css" rel="stylesheet"/>
    <script src="https://unpkg.com/dropzone"></script>
    <script src="https://unpkg.com/cropperjs"></script>

    <!-- Bootstrap CSS -->
    <!--<link rel="stylesheet" href="../../assets/css/bootstrap.min.css">-->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">
    <link rel="stylesheet" href="/assets/css/styleSheet.css">
    <link rel="icon" href="../../img/wc.png" type="image/png">
    <title>PitaOffice</title>

    <style>
        :root {
            --amareloPita: #DDBD0C;
        }

        .image_area {
            position: relative;
        }

        .preview {
            overflow: hidden;
            width: 160px;
            height: 160px;
            margin: 10px;
            border: 1px solid red;
        }

        .modal-lg {
            max-width: 1000px !important;
        }

        .overlay {
            position: absolute;
            bottom: 10px;
            left: 0;
            right: 0;
            background-color: rgba(255, 255, 255, 0.5);
            overflow: hidden;
            height: 0;
            transition: .5s ease;
            width: 100%;
        }

        .image_area:hover .overlay {
            height: 50%;
            cursor: pointer;
        }

        img {
            display: block;
            max-width: 100%;
        }

        .text {
            color: #333;
            font-size: 20px;
            position: absolute;
            top: 50%;
            left: 50%;
            -webkit-transform: translate(-50%, -50%);
            -ms-transform: translate(-50%, -50%);
            transform: translate(-50%, -50%);
            text-align: center;
        }
    </style>
</head>

<body>

<svg xmlns="http://www.w3.org/2000/svg" style="display: none;">
    <symbol id="home" viewBox="0 0 16 16" fill="currentColor">
        <path d="M8.354 1.146a.5.5 0 0 0-.708 0l-6 6A.5.5 0 0 0 1.5 7.5v7a.5.5 0 0 0 .5.5h4.5a.5.5 0 0 0 .5-.5v-4h2v4a.5.5 0 0 0 .5.5H14a.5.5 0 0 0 .5-.5v-7a.5.5 0 0 0-.146-.354L13 5.793V2.5a.5.5 0 0 0-.5-.5h-1a.5.5 0 0 0-.5.5v1.293L8.354 1.146zM2.5 14V7.707l5.5-5.5 5.5 5.5V14H10v-4a.5.5 0 0 0-.5-.5h-3a.5.5 0 0 0-.5.5v4H2.5z"/>
    </symbol>
    <symbol id="people-circle" viewBox="0 0 16 16" fill="currentColor">
        <path d="M11 6a3 3 0 1 1-6 0 3 3 0 0 1 6 0z"/>
        <path fill-rule="evenodd"
              d="M0 8a8 8 0 1 1 16 0A8 8 0 0 1 0 8zm8-7a7 7 0 0 0-5.468 11.37C3.242 11.226 4.805 10 8 10s4.757 1.225 5.468 2.37A7 7 0 0 0 8 1z"/>
    </symbol>
    <symbol id="table" viewBox="0 0 16 16" fill="currentColor">
        <path d="M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm15 2h-4v3h4V4zm0 4h-4v3h4V8zm0 4h-4v3h3a1 1 0 0 0 1-1v-2zm-5 3v-3H6v3h4zm-5 0v-3H1v2a1 1 0 0 0 1 1h3zm-4-4h4V8H1v3zm0-4h4V4H1v3zm5-3v3h4V4H6zm4 4H6v3h4V8z"/>
    </symbol>
    <symbol id="display" viewBox="0 0 16 16" fill="currentColor">
        <path d="M0 4s0-2 2-2h12s2 0 2 2v6s0 2-2 2h-4c0 .667.083 1.167.25 1.5H11a.5.5 0 0 1 0 1H5a.5.5 0 0 1 0-1h.75c.167-.333.25-.833.25-1.5H2s-2 0-2-2V4zm1.398-.855a.758.758 0 0 0-.254.302A1.46 1.46 0 0 0 1 4.01V10c0 .325.078.502.145.602.07.105.17.188.302.254a1.464 1.464 0 0 0 .538.143L2.01 11H14c.325 0 .502-.078.602-.145a.758.758 0 0 0 .254-.302 1.464 1.464 0 0 0 .143-.538L15 9.99V4c0-.325-.078-.502-.145-.602a.757.757 0 0 0-.302-.254A1.46 1.46 0 0 0 13.99 3H2c-.325 0-.502.078-.602.145z"/>
    </symbol>
</svg>

<div class="container-fluid p-0 m-0">
    <div class="row p-0 m-0">
        <div class="col-12 col-md-3 col-lg-2 p-0 m-0 bg-dark" id="sticky-sidebar">
            <div class="d-flex flex-column flex-shrink-0 p-3 text-white">
                <a href="home.php" class="d-flex align-items-center mb-3 mb-md-0 me-md-auto text-white text-decoration-none">
                    <!--<svg class="bi me-2" width="40" height="32"><use xlink:href="#bootstrap"/></svg>-->
                    <img src="../../img/wc.png" class="bi me-2" width="40" alt="wc">
                    <span class="fs-4">PitaOffice</span>
                </a>
                <hr>
                <ul class="nav nav-pills flex-column mb-auto">
                    <li class="nav-item">
                        <a href="home.php"
                           class="nav-link text-white <?php if ($pagina == "/projects/siteorxestra/backoffice/system/home.php") echo "active" ?>"
                           aria-current="page">
                            <svg class="bi me-2" width="16" height="16">
                                <use xlink:href="#home"/>
                            </svg>
                            Home
                        </a>
                    </li>
                    <div class="dropdown">
                        <a href="#"
                           class="nav-link text-white <?php if ($pagina == "/projects/siteorxestra/backoffice/system/registarMembro.php" or $pagina == '/projects/siteorxestra/backoffice/system/membros.php') echo "active" ?>"
                           id="dropdownUser1" data-bs-toggle="dropdown" aria-expanded="false">
                            <svg class="bi me-2" width="16" height="16">
                                <use xlink:href="#table"/>
                            </svg>
                            Membros
                        </a>
                        <ul class="dropdown-menu dropdown-menu-dark text-small shadow" aria-labelledby="dropdownUser1">
                            <li><a class="dropdown-item" href="membros.php">Consultar</a></li>
                            <?php if ($_SESSION['acesso'] == 1) echo '<li><a class="dropdown-item" href="registarMembro.php">Adicionar</a></li>' ?>
                        </ul>
                    </div>
                    <?php if ($_SESSION['acesso'] == 1) { ?>
                        <li class="nav-item">
                            <a href="utilizadores.php"
                               class="nav-link text-white <?php if ($pagina == "/projects/siteorxestra/backoffice/system/registarUtilizador.php") echo "active" ?>">
                                <svg class="bi me-2" width="16" height="16">
                                    <use xlink:href="#people-circle"/>
                                </svg>
                                Utilizadores
                            </a>
                        </li>
                        <?php
                    }
                    ?>
                    <li class="nav-item text-white">
                        <a href="/" class="nav-link text-white">
                            <svg class="bi me-2" width="16" height="16">
                                <use xlink:href="#display"/>
                            </svg>
                            Front-end
                        </a>
                    </li>
                </ul>
                <hr>
                <div class="dropdown">
                    <a href="#" class="d-flex align-items-center text-white text-decoration-none dropdown-toggle"
                       id="dropdownUser2" data-bs-toggle="dropdown" aria-expanded="false">
                        <img src="<?php echo $utilizador->img ?>" alt="" width="32" height="32"
                             class="rounded-circle me-2">
                        <strong><?php echo $utilizador->alcunha ?></strong>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-dark text-small shadow" aria-labelledby="dropdownUser1">
                        <?php
                        if ($_SESSION['acesso'] == 1)
                            echo '<li><a class="dropdown-item" href="logAcesso.php">Log de Acesso</a></li>';
                        ?>
                        <li class="nav-item">
                            <a class="dropdown-item" href="editarUtilizador.php">Editar <?php echo $utilizador->alcunha ?></a>
                        </li>
                        <li class="nav-item">
                            <hr class="dropdown-divider">
                        </li>
                        <li class="nav-item">
                            <a class="dropdown-item" href="../index.php">Sair</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-12 col-md-9 col-lg-10 pb-5 p-0">