<?php
session_start();

//verifica se há uma sessão activa
if (!isset($_SESSION['idUtilizador']) or $_SESSION['acesso'] != 1) {
    header('location: ../index.php');
}

//Chama o ficheiro com as funções do PHP
require '../php/functions.php';
include 'header.php';
?>

<main>
    <div class="container mt-5 align-center">
        <div class="col-8 offset-lg-2">
            <div class="row">
                <div class="col-12">
                    <h3>Registar Utilizador</h3>
                </div>
                <form action="#" method="post" name="formRegistaUtilizador">
                    <div class="row">
                        <div class="col-12">
                            <div class="col-sm-12 col-md-12 col-lg-4 image_area mx-auto text-center">
                                <label class="form-label" for="upload_image">
                                    <img class="img-fluid rounded-circle" width="100" src="img/person.svg"
                                         alt="Fotografia de perfil"
                                         id="uploaded_image">
                                    <div class="overlay">
                                        <p>Clique para alterar a imagem</p>
                                    </div>
                                    <input class="form-control form-control-sm" type="file" id="upload_image"
                                           name="image"
                                           accept="image/jpeg, image/png" style="display: none">
                                </label>
                            </div>
                            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
                                 aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title">Corte a imagem antes de a carregar</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">×</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <div class="img-container">
                                                <div class="row">
                                                    <div class="col-md-8">
                                                        <img style="max-width: 500px" src="" id="sample_image"/>
                                                    </div>
                                                    <div class="col-md-4">
                                                        <div class="preview"></div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" id="crop" class="btn btn-primary">Cortar</button>
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                                Cancelar
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-12 col-md-6 col-lg-4 ">
                            <label for="nome">Nome</label>
                            <input type="text" name="nome" class="form-control" id="nome" placeholder="Inserir nome">
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <label for="nome">Apelido</label>
                            <input type="text" name="apelido" class="form-control" id="apelido"
                                   placeholder="Inserir apelido">
                        </div>
                        <div class="col-12 col-md-6 col-lg-4">
                            <label for="alcunha">Alcunha</label>
                            <input type="text" name="alcunha" class="form-control" id="alcunha"
                                   placeholder="Inserir alcunha">
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-12 col-md-6">
                            <label for="email">Email</label>
                            <input type="email" name="email" class="form-control" id="email"
                                   placeholder="Inserir Email">
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="password">Password</label>
                            <input type="password" name="pass" class="form-control" id="pass"
                                   placeholder="Inserir Password" onkeyup="check()">
                        </div>
                        <div class="col-12 col-md-6">
                            <label for="confirmaPass">Confirme a Password</label>
                            <input type="password" name="confirmaPassword" id="confirmaPass" class="form-control"
                                   placeholder="Confirma Password" onkeyup="check()">
                            <span id="message"></span>
                        </div>
                        <div class="col-12 mt-3">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" id="admin" name="admin" value="1">
                                <label class="form-check-label">Admin</label>
                            </div>
                        </div>
                    </div>
                    <div class="btn-group">
                        <button type="submit" class="btn btn-primary mt-3" name="btnRegistarUtilizador">Criar</button>
                        <button type="button" class="btn btn-secondary mt-3" name="btnVoltar" onclick="location.href='utilizadores.php'">Voltar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>

<?php
include 'footer.html';
?>

<script>
    //    Script responsável pelo corte da imagem

    $(document).ready(function () {
        var $modal = $('#modal');
        var image = document.getElementById('sample_image');
        var cropper;

        $('#upload_image').change(function (event) {
            var files = event.target.files;
            var done = function (url) {
                image.src = url;
                $modal.modal('show');
            };

            if (files && files.length > 0) {
                reader = new FileReader();
                reader.onload = function (event) {
                    done(reader.result);
                };
                reader.readAsDataURL(files[0]);
            }
        });

        //Triguer do modal
        $modal.on('shown.bs.modal', function () {
            cropper = new Cropper(image, {
                aspectRatio: 1,
                viewMode: 3,
                preview: '.preview'
            });
        }).on('hidden.bs.modal', function () {
            cropper.destroy();
            cropper = null;
        });

        $('#crop').click(function () {
            canvas = cropper.getCroppedCanvas({
                width: 400,
                height: 400
            });

            canvas.toBlob(function (blob) {
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    var base64data = reader.result;
                    $.ajax({
                        url: 'upload.php',
                        method: 'POST',
                        data: {image: base64data},
                        success: function (data) {
                            $modal.modal('hide');
                            $('#uploaded_image').attr('src', data);
                        }
                    });
                };
            });
        });

    });

    var check = function () {
        if (document.getElementById('pass').value == document.getElementById('confirmaPass').value) {
            document.getElementById('message').style.color = 'green';
            document.getElementById('message').innerHTML = 'matching';
        } else {
            document.getElementById('message').style.color = 'red';
            document.getElementById('message').innerHTML = 'not matching';
        }
    }
</script>

