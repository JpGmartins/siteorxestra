<?php

session_start();

if (!isset($_SESSION['idUtilizador']) or $_SESSION['acesso'] != 1) {
    header('location: ../index.php');
}

include 'header.php';
include '../php/functions.php';
?>
<main>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <h3>Utilizadores</h3>
                <button type="button" name="btnRegistaUtilizador" class="btn btn-light" onclick="location.href='registarUtilizador.php'">Novo Utilizador</button>
            </div>
            <?php
            echo imprimeUtilizadores();
            ?>
        </div>
    </div>
</main>
<?php
require 'footer.html';
?>