<?php
session_start();

if (!isset($_SESSION['idUtilizador'])) {
    header('location: ../index.php');
}

include 'header.php';
include '../php/functions.php';
?>
<script>
    function validaForm() {
        var pass = document.forms["formAlteraPassUtilizador"]["novaPass"].value;
        var novaPass = document.forms["formAlteraPassUtilizador"]["confNovaPass"].value;

        if (pass != novaPass) {
            alert("As palavras-passe não coicidem...")
            return false;
        }
    }
</script>
<main>
    <div class="container mt-5">
        <div class="col-12">
            <div class="row">
                <div class="col-12">
                    <h3>Editar Utilizador</h3>
                    <h5><?php echo $utilizador->nome . " \"" . $utilizador->alcunha . "\" " . $utilizador->apelido ?></h5>
                </div>
            </div>
            <form action="#" name="formAlteraPassUtilizador" method="post" onsubmit="validaForm()">
                <div class="row">
                    <div class="col-12 pb-3">
                        <div class="col-sm-12 col-md-12 col-lg-4 image_area mx-auto text-center">
                            <label class="form-label" for="upload_image">
                                <img class="img-fluid rounded-circle" width="100" src="<?php echo $utilizador->img?>"
                                     alt="Fotografia de perfil"
                                     id="uploaded_image">
                                <div class="overlay">
                                    <p>Clique para alterar a imagem</p>
                                </div>
                                <input class="form-control form-control-sm" type="file" id="upload_image"
                                       name="image"
                                       accept="image/jpeg, image/png" style="display: none">
                            </label>
                        </div>
                        <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
                             aria-hidden="true">
                            <div class="modal-dialog modal-lg" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Cortar imagem antes de carregar</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">×</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <div class="img-container">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <img style="max-width: 500px" src="" id="sample_image"/>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="preview"></div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" id="crop" class="btn btn-primary">Cortar</button>
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">
                                            Cancelar
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-md-4 offset-md-4">
                        <label for="passActual">Palavra-passe actual</label>
                        <input class="form-control" type="password" name="passActual" id="passActual" max="15" required>
                    </div>
                    <div class="col-12 col-md-4 offset-md-2">
                        <label for="novaPass">Nova palavra-passe</label>
                        <input type="password" name="novaPass" id="novaPass" minlength="6" maxlength="15"
                               class="form-control" required>
                    </div>
                    <div class="col-12 col-md-4">
                        <label for="confNovaPass">Confirma a nova palavra-passe</label>
                        <input type="password" name="confNovaPass" id="confNovaPass" minlength="6" maxlength="15"
                               class="form-control">
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-4 offset-md-2">
                        <input type="submit" name="submmitNovaPass" id="submitNovaPass" class="btn btn-primary"
                               value="Alterar">
                    </div>
                </div>
            </form>
        </div>
    </div>
</main>
<?php
include 'footer.html';
?>
<script>
    //    Script responsável pelo corte da imagem

    $(document).ready(function () {
        var $modal = $('#modal');
        var image = document.getElementById('sample_image');
        var cropper;

        $('#upload_image').change(function (event) {
            var files = event.target.files;
            var done = function (url) {
                image.src = url;
                $modal.modal('show');
            };

            if (files && files.length > 0) {
                reader = new FileReader();
                reader.onload = function (event) {
                    done(reader.result);
                };
                reader.readAsDataURL(files[0]);
            }
        });

        //Triguer do modal
        $modal.on('shown.bs.modal', function () {
            cropper = new Cropper(image, {
                aspectRatio: 1,
                viewMode: 3,
                preview: '.preview'
            });
        }).on('hidden.bs.modal', function () {
            cropper.destroy();
            cropper = null;
        });

        $('#crop').click(function () {
            canvas = cropper.getCroppedCanvas({
                width: 400,
                height: 400
            });

            canvas.toBlob(function (blob) {
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    var base64data = reader.result;
                    $.ajax({
                        url: 'upload.php',
                        method: 'POST',
                        data: {image: base64data},
                        success: function (data) {
                            $modal.modal('hide');
                            $('#uploaded_image').attr('src', data);
                        }
                    });
                };
            });
        });

    });
</script>
