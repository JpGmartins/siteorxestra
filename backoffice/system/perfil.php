<?php
session_start();

//Guarda o id do membro selecionado numa variável de sessão, para que possa ser utilizado ao longo da aplicação
$_SESSION['idMembro'] = $_GET['idMembro'];

//Verifica se há uma sessão iniciada
if (!isset($_SESSION['idUtilizador'])) {
    header('location: ../index.php');
}

// inclui o header da página
include 'header.php';
//Chama a classe Membro
require_once '../php/Membro.php';
?>
<main>
    <div class="container mt-5">
        <div class="row">
            <div class="col-12 col-md-2 col-lg-3">
                <img class="img-fluid rounded mx-auto" style="min-width: 150px" src="<?php echo $membro->fotoPerfil ?>"
                     alt="Foto de perfil" id="fotoPerfil" data-holder-rendered="true">
            </div>
            <div class="col-10 col-md-10 col-lg-9">
                <div class="row">
                    <div class="col-12">
                        <h2><?php echo $membro->alcunha;
                            if ($_SESSION['acesso'] == 1) {
                                ?>
                                <button style="border: none; background: none"
                                        onclick="location.href='editarMembro.php?idMembro=<?php echo $_SESSION['idMembro'] ?>'">
                                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor"
                                         class="bi bi-pencil-square" viewBox="0 0 16 16">
                                        <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                                        <path fill-rule="evenodd"
                                              d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                                    </svg>
                                </button>
                            <?php } ?>
                        </h2>
                        <p><?php echo $membro->anoEntrada ?> -
                            <?php
                            $estado = $membro->estado;
                            switch ($estado) {
                                case 0:
                                    $estado = 'Falecido';
                                    break;
                                case 1:
                                    $estado = 'Velho';
                                    break;
                                case 2:
                                    $estado = 'Activo';
                            }
                            echo $estado;
                            ?>
                        </p>
                    </div>
                    <div class="col-6">
                        <h6>Nome completo</h6>
                        <p><?php echo $membro->nome ?></p>
                    </div>
                    <div class="col-6">
                        <h6>CC</h6>
                        <p><?php echo $membro->cc ?></p>
                    </div>
                    <hr>
                    <div class="col-12">
                        <h5>Contactos</h5>
                        <h6>E-Mail</h6>
                        <p><?php echo $membro->email ?></p>
                    </div>
                    <div class="col-6">
                        <h6>Telefone 1</h6>
                        <p><?php echo $membro->telefone1 ?></p>
                    </div>
                    <div class="col-6">
                        <h6>Telefone 2</h6>
                        <p><?php echo $membro->telefone2 ?></p>
                    </div>
                    <hr>
                    <div class="col-12">
                        <h5>Morada</h5>
                        <p><?php echo $membro->rua ?><br><?php echo $membro->codPostal ?>
                            , <?php echo $membro->localidade ?><br><?php echo $membro->pais ?></p>
                    </div>
                    <?php if ($membro->descricao != null) echo "<hr><div class='col-12'><h6>Descrição</h6><p>" . $membro->descricao . "</p></div>"; ?>
                </div>
                <button type="button" class="btn btn-primary" onclick="location.href='membros.php'">Voltar</button>
            </div>
        </div>
    </div>
</main>
<?php
require_once 'footer.html';
?>
