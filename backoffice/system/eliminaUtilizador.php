<?php
session_start();
//Verifica se há sessão iniciada
if (!isset($_SESSION['idUtilizador'])) {
    header('location: ../index.php');
}

require '../php/functions.php';

$idUtilizador = $_GET['eliminaIdUtilizador'];
if(eliminaUtilizador($idUtilizador)){
    header('location: utilizadores.php');
}else
    echo "Ocorreu um erro";

