<?php
session_start();
if ($_SESSION['acesso'] == 1) {
    ?>
    <!DOCTYPE html>
    <html lang="pt">
    <head>
        <meta charset="utf-8">
        <title>Log de Acesso</title>
        <style>
            table {
                font-family: arial, sans-serif;
                border-collapse: collapse;
                width: 100%;
            }

            td, th {
                border: 1px solid #dddddd;
                text-align: left;
                padding: 8px;
            }

            tr:nth-child(even) {
                background-color: #dddddd;
            }
        </style>
    </head>
    <body>
    <a href="home.php" class="link-success link">Voltar</a>
    <table>
        <tr>
            <th>Alcunha</th>
            <th>Data de Login</th>
            <th>Data de Logout</th>
            <th>User Agent</th>
        </tr>

        <?php
//        Conexão há base de dados
        require '../php/connectDB.php';

//        Query à BD
        $sql = "SELECT utilizadores.alcunha AS 'Alcunha', log.data_login AS 'Data de Login', log.data_logout AS 'Data de Logout', log.agente AS 'agente' FROM orxestra_pitagorica.log INNER JOIN orxestra_pitagorica.utilizadores on log.id_utilizador = utilizadores.id_utilizador ORDER BY id_log DESC";
        $result = $bd->query($sql);

//        Imprime resultados numa tabela
        while ($row = $result->fetch_assoc()) {
            echo "<tr><td>" . $row['Alcunha'] . "</td><td>" . $row['Data de Login'] . "</td><td>" . $row['Data de Logout'] . "</td><td>".$row['agente']."</td></tr>";
        }
        ?>
    </table>
    </body>
    </html>
    <?php
} else
    echo "Acesso negado";
?>