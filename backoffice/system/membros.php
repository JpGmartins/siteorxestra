<?php
session_start();
//Verifica se há sessão iniciada
if (!isset($_SESSION['idUtilizador'])) {
    header('location: ../index.php');
}

require '../php/functions.php';

$trClass = 'class="table-light"';

require 'header.php';
?>
    <main>
        <div class="container-fluid">
            <div class="row mt-5">
                <div class="col">
                    <h3>Membros</h3>
                </div>
            </div>
            <form action="#" method="post">
                <div class="row mt-3">
                    <div class="col-10 col-lg-4">
                        <label hidden for="pesquisa">Pesquisa</label>
                        <div class="input-group">
                            <input type="search" name="pesquisa" id="pesquisa" class="form-control"
                                   placeholder="Pesquisar">
                            <button type="submit" class="btn btn-primary" name="btnPesquisa" id="btnPesquisa">
                                Pesquisar
                            </button>
                        </div>

                        <div class="form-check mt-3">
                            <input class="form-check-input" type="checkbox" value="0" id="falecido"
                                   name="estadoFalecido">
                            <label class="form-check-label" for="falecido">Falecido</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" id="velho" name="estadoVelho">
                            <label class="form-check-label" for="velho">Velho</label>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="2" id="activo" name="estadoActivo">
                            <label class="form-check-label" for="activo">Activo</label>
                        </div>
                    </div>
                    <div class="col-2 col-lg-2 offset-lg-6 text-center">
                        <button type="button" class="btn btn-primary" name="btnPDF" id="btnPDF"
                                onclick="location.href='../assets/TCPDF/gerarPDF.php'">PDF
                        </button>
                    </div>
                </div>

            </form>
            <div class="table-responsive mt-3">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th scope="col">Ano Entrada</th>
                        <th scope="col">Alcunha</th>
                        <th scope="col">Nome</th>
                        <th scope="col">Estado</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php echo pesquisa() ?>
                    </tbody>
                </table>
            </div>

        </div>
    </main>
<?php
require 'footer.html';