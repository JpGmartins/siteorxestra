<?php
session_start();

//Verifica se há uma sessão inciada
if (!isset($_SESSION['idUtilizador']) or $_SESSION['acesso'] != 1) {
    header('location: ../index.php');
}

//Se um mebro for inserido com sucesso, surge um alerta que informa o utilizador
if (isset($_GET['resultado'])) echo('<script>alert("Membro adicionado à Base de Dados.")</script>');

//Chama o ficheiro de funções do PHP e o header da página
include '../php/functions.php';
include 'header.php';
?>

<main>
    <div class="container mt-5">
        <div class="col-12">
            <h3>Registar Membro</h3>
        </div>
        <form action="#" method="post" enctype="multipart/form-data">
            <div class="row">
                <div class="col-sm-12 col-md-12 col-lg-4 text-center image_area">
                    <label class="form-label" for="upload_image">
                        <img class="img-fluid rounded-circle" width="100" src="img/person.svg"
                             alt="Fotografia de perfil"
                             id="uploaded_image">
                        <div class="overlay">
                            <p>Clique para alterar a imagem</p>
                        </div>
                        <input class="form-control form-control-sm" type="file" id="upload_image" name="image"
                               accept="image/jpeg, image/png" style="display: none">
                    </label>
                </div>
                <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel"
                     aria-hidden="true">
                    <div class="modal-dialog modal-lg" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title">Corte a imagem antes de a caregar</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">×</span>
                                </button>
                            </div>
                            <div class="modal-body">
                                <div class="img-container">
                                    <div class="row">
                                        <div class="col-md-8">
                                            <img style="max-width: 500px" src="" id="sample_image"/>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="preview"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" id="crop" class="btn btn-primary">Cortar</button>
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <label class="form-label" for="alcunha">Alcunha</label>
                    <input type="text" id="alcunha" name="alcunha" class="form-control" placeholder="Alcunha">
                    <label class="form-label" for="nome">Nome</label>
                    <input type="text" name="nome" id="nome" class="form-control" placeholder="Nome completo">
                </div>
                <div class="col-sm-12 col-md-6 col-lg-4">
                    <label class="form-label" for="cc">CC</label>
                    <input type="number" max="999999999" placeholder="16377429" name="cc" id="cc" class="form-control">
                    <label class="form-label" for="anoEntrada">Ano de entrada</label>
                    <input type="number" min="1980" max="9999" placeholder="2018" name="anoEntrada" id="anoEntrada"
                           class="form-control" required>
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-md-12 col-lg-6">
                    <label class="form-label" for="email">Email</label>
                    <input type="email" class="form-control" name="email" id="email">
                </div>
                <div class="col-md-6 col-lg-3">
                    <label class="form-label" for="tel1">Telefone 1</label>
                    <input type="tel" name="tel1" id="tel1" class="form-control">
                </div>
                <div class="col-md-6 col-lg-3">
                    <label class="form-label" for="tel2">Telefone 2</label>
                    <input type="tel" name="tel2" id="tel2" class="form-control">
                </div>
            </div>
            <div class="row mt-4">
                <div class="col-12">
                    <label class="form-label" for="rua">Rua</label>
                    <input type="text" name="rua" id="rua" class="form-control">
                </div>
                <div class="col-md-4 col-lg-2">
                    <label class="form-label" for="codPostal">Código Postal</label>
                    <input type="text" class="form-control" name="codPostal" id="codPostal" placeholder="0000-000">
                </div>
                <div class="col-md-8 col-lg-5">
                    <label class="form-label" for="localidade">Localidade</label>
                    <input type="text" name="localidade" id="localidade" class="form-control" placeholder="Localidade">
                </div>
                <div class="col-md-12 col-lg-5">
                    <label class="form-label" for="pais">País</label>
                    <input type="text" name="pais" id="pais" class="form-control" placeholder="País">
                </div>
            </div>
            <div class="row mt-4 align-items-center">
                <div class="col col-lg-8">
                    <label class="form-label" for="descricao">Descrição</label>
                    <textarea class="form-control" name="descricao" id="descricao" placeholder="Descrição"
                              rows="6"></textarea>
                </div>
                <div class="col col-lg-4 align-middle">
                    <div class="form-check">
                        <label class="form-check-label" for="estadoFalecido">Falecido</label>
                        <input class="form-check-input" type="radio" id="estadoFalecido" name="estado" value="0">
                    </div>
                    <div class="form-check">
                        <label class="form-check-label" for="estadoVelho">Velho</label>
                        <input type="radio" class="form-check-input" id="estadoVelho" name="estado" value="1">
                    </div>
                    <div class="form-check">
                        <label class="form-check-label" for="estadoActivo">Activo</label>
                        <input class="form-check-input" type="radio" name="estado" id="estadoActivo" value="2" checked>
                    </div>
                </div>
            </div>
            <div class="btn-group mt-4">
                <button type="submit" name="btnRegistarMembro" class="btn btn-primary">Registar</button>
                <button type="reset" name="reset" class="btn btn-secondary">Limpar</button>
                <button type="button" name="cancel" class="btn btn-dark" onclick="location.href='membros.php'">Voltar
                </button>
            </div>
        </form>
    </div>
</main>
<?php
require 'footer.html';
?>
<script>

    $(document).ready(function () {

        var $modal = $('#modal');

        var image = document.getElementById('sample_image');

        var cropper;

        $('#upload_image').change(function (event) {
            var files = event.target.files;

            var done = function (url) {
                image.src = url;
                $modal.modal('show');
            };

            if (files && files.length > 0) {
                reader = new FileReader();
                reader.onload = function (event) {
                    done(reader.result);
                };
                reader.readAsDataURL(files[0]);
            }
        });

        $modal.on('shown.bs.modal', function () {
            cropper = new Cropper(image, {
                aspectRatio: 1,
                viewMode: 3,
                preview: '.preview'
            });
        }).on('hidden.bs.modal', function () {
            cropper.destroy();
            cropper = null;
        });

        $('#crop').click(function () {
            canvas = cropper.getCroppedCanvas({
                width: 400,
                height: 400
            });

            canvas.toBlob(function (blob) {
                url = URL.createObjectURL(blob);
                var reader = new FileReader();
                reader.readAsDataURL(blob);
                reader.onloadend = function () {
                    var base64data = reader.result;
                    $.ajax({
                        url: '../php/upload.php',
                        method: 'POST',
                        data: {image: base64data},
                        success: function (data) {
                            $modal.modal('hide');
                            $('#uploaded_image').attr('src', data);
                        }
                    });
                };
            });
        });

    });
</script>
