<p align="center">
  <a href="" rel="noopener">
 <img width=200px height=200px src="https://i.imgur.com/6wj0hh6.jpg" alt="Project logo"></a>
</p>

<h3 align="center">Orxestra Pitagórica On Web</h3>

<div align="center">

[![Status](https://img.shields.io/badge/status-active-success.svg)]()
[![GitHub Issues](https://img.shields.io/github/issues/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/issues)
[![GitHub Pull Requests](https://img.shields.io/github/issues-pr/kylelobo/The-Documentation-Compendium.svg)](https://github.com/kylelobo/The-Documentation-Compendium/pulls)
[![License](https://img.shields.io/badge/license-MIT-blue.svg)](/LICENSE)

</div>

---

<p align="center"> Este projecto tem como objectivo a criação de uma página web para a Orxestra Pitagórica da Secção de Fado da Associação Académica de Coimbra.
    <br> 
</p>

## 📝 Table of Contents

- [About](#about)
- [Getting Started](#getting_started)
- [Deployment](#deployment)
- [Usage](#usage)
- [Built Using](#built_using)
- [TODO](../TODO.md)
- [Contributing](../CONTRIBUTING.md)
- [Authors](#authors)
- [Acknowledgments](#acknowledgement)

## 🧐 About <a name = "about"></a>

Este projecto tem como objectivo a criação de uma página web para a Orxestra Pitagórica da Secção de Fado da Associação
Académica de Coimbra. Contará possivelmente no futuro com uma loja online.

## 🏁 Getting Started <a name = "getting_started"></a>

Desenvolvido em html e css com bootstrap.

## 🎈 Usage <a name="usage"></a>

Navegação standard de uma página web

## ⛏️ Built Using <a name = "built_using"></a>

- [MongoDB](https://www.mongodb.com/) - Database
- [Express](https://expressjs.com/) - Server Framework
- [VueJs](https://vuejs.org/) - Web Framework
- [NodeJs](https://nodejs.org/en/) - Server Environment

## ✍️ Authors <a name = "authors"></a>

- João Pedro Martins - Idea & Initial work

## Next Steps

- Adicionar marcos importantes na timeline;
- Menu com as letras;
- Secção dedicada aos membros activos;
- Secção de consultório sentimental, onde a orxestra responde a dúvidas sobre variadas temáticas, não só mas também:
  enologia, vulcanologia, sexologia, meteorologia, alergia, siderurgia,
- Fotos rotativas na galeria

## 🎉 Acknowledgements <a name = "acknowledgement"></a>

- Orxestra Pitagórica
- Pedro Valente
- João Pedro Martins
